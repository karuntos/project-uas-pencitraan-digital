unit outputImage;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
    ExtDlgs;

type

    { TOutputForm }

    TOutputForm = class(TForm)
        btnSave: TButton;
        imgOutput: TImage;
        dlgSpic: TSavePictureDialog;
        ScrollBox1: TScrollBox;
        procedure btnSaveClick(Sender: TObject);
    private

    public

    end;

var
    OutputForm: TOutputForm;

implementation

{$R *.lfm}

{ TOutputForm }

procedure TOutputForm.btnSaveClick(Sender: TObject);
begin
    if dlgSpic.Execute then
       imgOutput.Picture.SaveToFile(dlgSpic.FileName);
end;

end.

