unit main;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
    ExtDlgs, thresholdset, outputImage, Lclintf;

type

    { TMainForm }

    TMainForm = class(TForm)
        btnLoadPattern1,
        btnLoadPattern2: TButton;

        btnLoadTexture1,
        btnLoadTexture2: TButton;


        imgPattern1,
        imgPattern2: TImage;

        imgTexture1,
        imgTexture2: TImage;

        btnCreateBatik: TButton;

        Label1, Label2,
        Label3, Label4,
        lblStatus: TLabel;

        dlgOpicPattern1,
        dlgOpicPattern2: TOpenPictureDialog;

        dlgOpicTexture1,
        dlgOpicTexture2: TOpenPictureDialog;

        ScrollBox1: TScrollBox;
        ScrollBox2: TScrollBox;

        procedure btnCreateBatikClick(Sender: TObject);
        procedure btnLoadPattern1Click(Sender: TObject);
        procedure btnLoadPattern2Click(Sender: TObject);
        procedure btnLoadTexture1Click(Sender: TObject);
        procedure btnLoadTexture2Click(Sender: TObject);
    private
        pattern1,
        pattern2 : TBitmap;

        texture1,
        texture2 : TBitmap;

        batik : TBitmap;

        pattern1Width, pattern1Height,

        pattern2Width, pattern2Height,

        texture1Width, texture1Height,

        texture2Width, texture2Height,

        batikWidth, batikHeight
        : integer;

        function wh_eq(var bitmap: TBitmap): boolean;      // cek ukuran gambar apakah persegi
        function sqImg_gt32(var bitmap: TBitmap): boolean; // cek apakah gambar persegi melebihi 32x32
    public
        function tileAndRotate() : TBitmap;  // tiling menggunakan pattern1
        function stamp(var bitmap: TBitmap; rows, cols: integer): TBitmap; // stamping sebanyak 15 baris dan 15 kolom
        procedure colorizeForeground();      // warnai dengan texture1
        procedure colorizeBackground();      // warnai dengan texture2
        procedure arbrRotate(var inBitmap,outBitmap: TBitmap; angleDegree: integer);
    end;

var
    MainForm: TMainForm;

implementation

{$R *.lfm}       {$RANGECHECKS ON}

{ TMainForm }

procedure TMainForm.btnLoadPattern1Click(Sender: TObject);
begin
     if assigned(pattern1) then freeandnil(pattern1);
     if dlgOpicPattern1.Execute then begin
        pattern1 := TBitmap.Create;
        pattern1.LoadFromFile(dlgOpicPattern1.FileName);
        if not wh_eq(pattern1) then begin
           showmessage('Ukuran pola harus persegi!');
           freeandnil(pattern1);
           exit;
        end else if sqImg_gt32(pattern1) then begin
           showmessage('Ukuran pola maksimal adalah 32x32!');
           freeandnil(pattern1);
           exit;
        end;

        pattern1.getSize(pattern1Width, pattern1Height);

        pattern1 := ThresholdSetForm.executeImageThresholding(pattern1);

        // pasang 'pattern1' ke 'imgPattern1'
        imgPattern1.Picture.Bitmap.assign(pattern1);
     end;
end;

procedure TMainForm.btnLoadPattern2Click(Sender: TObject);
begin
     if assigned(pattern2) then freeandnil(pattern2);
     if dlgOpicPattern2.Execute then begin
        pattern2 := TBitmap.Create;
        pattern2.LoadFromFile(dlgOpicPattern2.FileName);
        if not wh_eq(pattern2) then begin
           showmessage('Ukuran pola harus persegi!');
           freeandnil(pattern2);
           exit;
        end else if sqImg_gt32(pattern2) then begin
           showmessage('Ukuran pola maksimal adalah 32x32!');
           freeandnil(pattern2);
           exit;
        end;

        pattern2.getSize(pattern2Width, pattern2Height);

        pattern2 := ThresholdSetForm.executeImageThresholding(pattern2);

        imgPattern2.Picture.Bitmap.assign(pattern2);
     end;
end;

procedure TMainForm.btnLoadTexture1Click(Sender: TObject);
begin
     if assigned(texture1) then freeandnil(texture1);
     if dlgOpicTexture1.Execute then begin
        texture1 := TBitmap.Create;
        texture1.LoadFromFile(dlgOpicTexture1.FileName);

        texture1.getSize(texture1Width, texture1Height);

        imgTexture1.Picture.Bitmap.assign(texture1);
     end;
end;

procedure TMainForm.btnLoadTexture2Click(Sender: TObject);
begin
     if assigned(texture2) then freeandnil(texture2);
     if dlgOpicTexture2.Execute then begin
        texture2 := TBitmap.Create;
        texture2.LoadFromFile(dlgOpicTexture2.FileName);

        texture2.getSize(texture2Width, texture2Height);

        imgTexture2.Picture.Bitmap.assign(texture2);
     end;
end;

procedure TMainForm.btnCreateBatikClick(Sender: TObject);
var patternJadi: TBitmap; centerx, centery: integer;
begin
     lblStatus.Caption := 'Proses';
     // tiling and rotating
     patternJadi := tileandrotate();

     // taruh 'pattern2' di tengah-tengah hasil
     centerx := (patternJadi.Width div 2) - (pattern2Width div 2);
     centery := (patternJadi.Height div 2) - (pattern2Height div 2);
     patternJadi.Canvas.Draw(centerx, centery, pattern2);

     // lakukan stamping dan hasilnya ditaruh ke 'batik'
     batik := stamp(patternJadi, 5, 5);
     batikWidth := batik.Width;
     batikHeight := batik.Height;

     colorizeBackground();

     colorizeForeGround();

     outputForm.imgOutput.Width := batikWidth;
     outputForm.imgOutput.Height := batikHeight;

     lblStatus.Caption := 'Siap';

     outputForm.imgOutput.Picture.Bitmap.assign(batik);
     outputForm.ShowModal;

     freeandnil(patternjadi);
     freeandnil(batik);
end;

function TMainForm.tileAndRotate() : TBitmap;
var
    outBitmap : TBItmap; tmpBitmap : TBitmap;
begin
      outBitmap := TBItmap.Create;
      outBitmap.SetSize(pattern1Width * 2, pattern1Height * 2);

      tmpBitmap := TBItmap.Create;
      tmpBitmap.setSize(outBitmap.Width, outBitmap.Height);

      arbrRotate(pattern1, tmpBitmap, 0);
      outBitmap.Canvas.Draw(0, 0, tmpBitmap);

      arbrRotate(pattern1, tmpBitmap, 90);
      outBitmap.Canvas.Draw(pattern1Width, 0, tmpBitmap);

      arbrRotate(pattern1, tmpBitmap, 180);
      outBitmap.Canvas.Draw(pattern1Width, pattern1Height, tmpBitmap);

      arbrRotate(pattern1, tmpBitmap, 270);
      outBitmap.Canvas.Draw(0, pattern1Height, tmpBitmap);

      freeandnil(tmpBitmap);
      result := outBitmap;
end;

function TMainForm.stamp(var bitmap: TBitmap; rows, cols: integer): TBitmap;
var outputBitmap: TBitmap; x, y: integer;
begin
     outputBitmap := TBitmap.Create;
     outputBitmap.SetSize(bitmap.Width * rows, bitmap.Height * cols);

     for x := 0 to rows - 1 do
         for y := 0 to cols - 1 do
             outputBitmap.Canvas.Draw(x * bitmap.Width, y * bitmap.Height, bitmap);

     result := outputBitmap;
end;

procedure TMainForm.colorizeForeground();
var x, y, copyRow, copyCol: Integer;
    newTexture1 : TBitmap;
begin
     copyRow := 1;
     copyCol := 1;

     if texture1Width < batikWidth then
        copyRow := (batikWidth div texture1Width) + 1;

     if texture1Height < batikHeight then
        copyCol := (batikHeight div texture1Height) + 1;

     newTexture1 := stamp(texture1, copyRow, copyCol);

     for y := 0 to batikHeight - 1 do
         for x := 0 to batikWidth - 1 do
           if batik.Canvas.Pixels[x,y] = clBlack then
              batik.Canvas.Pixels[x,y] := newTexture1.Canvas.Pixels[x,y];
end;

procedure TMainForm.colorizeBackground();
var x, y, copyRow, copyCol: Integer;
    newTexture2 : TBitmap;
begin
     copyRow := 1;
     copyCol := 1;

     if texture2Width < batikWidth then
        copyRow := (batikWidth div texture2Width) + 1;

     if texture2Height < batikHeight then
        copyCol := (batikHeight div texture2Height) + 1;

     newTexture2 := stamp(texture2, copyRow, copyCol);

     for y := 0 to batikHeight - 1 do
         for x := 0 to batikWidth - 1 do
           if batik.Canvas.Pixels[x,y] = clWhite then
              batik.Canvas.Pixels[x,y] := newTexture2.Canvas.Pixels[x,y];
end;

procedure TMainForm.arbrRotate(var inBitmap,outBitmap: TBitmap; angleDegree: integer);
var
    x0, y0, x1, y1, x2, y2, xp, yp: integer; angleRadian: double;
    tmpBitmap : TBitmap;
begin
     tmpBitmap := TBitmap.Create();
     tmpBitmap.assign(inBItmap);

     x0 := inBitmap.Width div 2;
     y0 := inBitmap.Height div 2;
     angleRadian := angleDegree * pi / 180;

     for y1 := 0 to inBitmap.Height - 1 do
         for x1 := 0 to inBitmap.Width - 1 do begin
             xp := x1 - x0;
             yp := y1 - y0;
             x2 := round(cos(angleRadian) * xp - sin(angleRadian) * yp + x0);
             y2 := round(sin(angleRadian) * xp + cos(angleRadian) * yp + y0);
             outBitmap.Canvas.Pixels[x2, y2] := tmpBitmap.Canvas.Pixels[x1, y1];
         end;
     freeandnil(tmpbitmap);
end;

function TMainForm.wh_eq(var bitmap: TBitmap): boolean;
begin
     Result := (bitmap.Width = bitmap.Height);
end;

function TMainForm.sqImg_gt32(var bitmap: TBitmap): boolean;
begin
     Result := (bitmap.Width > 32);
end;

end.

