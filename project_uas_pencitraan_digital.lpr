program project_uas_pencitraan_digital;

{$mode objfpc}{$H+}

uses
    {$IFDEF UNIX}{$IFDEF UseCThreads}
    cthreads,
    {$ENDIF}{$ENDIF}
    Interfaces, // this includes the LCL widgetset
    Forms, main, thresholdset, outputImage
    { you can add units after this };

{$R *.res}

begin
    RequireDerivedFormResource:=True;
    Application.Title:='Pembuat batik';
    Application.Scaled:=True;
    Application.Initialize;
    Application.CreateForm(TMainForm, MainForm);
    Application.CreateForm(TThresholdSetForm, ThresholdSetForm);
    Application.CreateForm(TOutputForm, OutputForm);
    Application.Run;
end.

