unit thresholdset;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
    Spin, lcltype, lclintf;

type

    { TThresholdSetForm }

    TThresholdSetForm = class(TForm)
        btnApply: TButton;
        image1: TImage;
        Label1: TLabel;
        spinThreshold: TSpinEdit;
        procedure spinThresholdChange(Sender: TObject);
    private
        bmpHeight, bmpWidth : integer;
        originalBitmap : TBitmap;
        outputBitmap   : TBitmap;

        procedure generateImageThreshold
                 (threshold: byte; var inBitmap, outBitmap: TBitmap);

    public
        function executeImageThresholding(var bitmap: TBitmap): TBitmap;
    end;

var
    ThresholdSetForm: TThresholdSetForm;

implementation

procedure TThresholdSetForm.generateImageThreshold
         (threshold: byte; var inBitmap, outBitmap: TBitmap);
var y, x : integer;
    redValue, greenValue, blueValue, grayValue: integer;
begin
  for y := 0 to bmpHeight - 1 do
      for x := 0 to bmpWidth - 1 do begin
        redValue := getRValue(inBitmap.Canvas.Pixels[x, y]);
        greenValue := getGValue(inBitmap.Canvas.Pixels[x, y]);
        blueValue := getBValue(inBitmap.Canvas.Pixels[x, y]);

        grayValue := (redValue + greenValue + blueValue) div 3;

        if grayValue > threshold then
          outBitmap.Canvas.Pixels[x, y] := RGB(255, 255, 255)
        else
          outBitmap.Canvas.Pixels[x, y] := RGB(0, 0, 0);
      end;
end;

procedure TThresholdSetForm.spinThresholdChange(Sender: TObject);
var value: integer;
    bmpHandle, maskHandle: HBitmap;
begin
  value := spinThreshold.Value;

  generateImageThreshold(value, originalBitmap, outputBitmap);

  // dapatkan handle outputBitmap
  bmpHandle  := outputBitmap.BitmapHandle;
  maskHandle := outputBitmap.MaskHandle;

  image1.Picture.Bitmap.LoadFromBitmapHandles(bmpHandle, MaskHandle);
end;

function TThresholdSetForm.executeImageThresholding(var bitmap: TBitmap): TBitmap;
var bmpHandle, maskHandle: HBitmap;
begin
  spinThreshold.value := 128;
  // dapatkan tinggi dan lebar bitmap
  bmpHeight  := bitmap.Height;
  bmpWidth   := bitmap.Width;
  // dapatkan handle bitmap
  bmpHandle  := bitmap.BitmapHandle;
  maskHandle := bitmap.MaskHandle;

  // buat original bitmap dan copy dari 'bitmap'
  originalBitmap := TBitmap.Create;
  originalBitmap.LoadFromBitmapHandles(bmpHandle, maskHandle);

  // buat output bitmap dan copy dari 'bitmap'
  outputBitmap := TBitmap.Create;
  outputBitmap.LoadFromBitmapHandles(bmpHandle, maskHandle);

  // nilai ambang awal 128
  generateImageThreshold(128, originalBitmap, outputBitmap);

  // dapatkan handle outputBitmap
  bmpHandle  := outputBitmap.bitmapHandle;
  maskHandle := outputBitmap.maskHandle;

  // copy bitmap ke image1
  image1.Picture.Bitmap.LoadFromBitmapHandles(bmpHandle, maskHandle);

  // tampilkan form
  thresholdSetForm.ShowModal;

  // setelah form ditutup...
  // dapatkan handle outputBitmap

  Result := outputBitmap;
end;

{$R *.lfm}

end.

